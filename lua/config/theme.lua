require("base16-colorscheme").with_config({
	telescope = false,
	indentblankline = true,
})

vim.cmd.colorscheme("base16-porple")
