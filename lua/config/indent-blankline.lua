require("ibl").setup({
	scope = { enabled = false },
	indent = {
		highlight = {
			"rainbowcol1",
			"rainbowcol2",
			"rainbowcol3",
			"rainbowcol4",
			"rainbowcol5",
			"rainbowcol6",
			"rainbowcol7",
		},
	},
})
